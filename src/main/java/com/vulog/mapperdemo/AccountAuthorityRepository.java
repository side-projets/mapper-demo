package com.vulog.mapperdemo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountAuthorityRepository extends JpaRepository<AccountAuthority, AccountAuthorityID> {
}
