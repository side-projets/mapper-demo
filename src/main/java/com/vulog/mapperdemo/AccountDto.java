package com.vulog.mapperdemo;

import java.util.Set;

public class AccountDto {
  String uuid;
  String username;

  Set<AccountAuthority> authorities;

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public Set<AccountAuthority> getAuthorities() {
    return authorities;
  }

  public void setAuthorities(Set<AccountAuthority> authorities) {
    this.authorities = authorities;
  }
}
