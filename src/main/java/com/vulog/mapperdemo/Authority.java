package com.vulog.mapperdemo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Authority {
  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  @Column(columnDefinition = "char(36)")
  private String uuid;

  @Column(unique = true)
  private String authority;

  @OneToMany(mappedBy = "authority", cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<AccountAuthority> accounts = new HashSet<>();

  public Authority() {
  }

  public Authority(String authority) {
    this.authority = authority;
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public String getAuthority() {
    return authority;
  }

  public void setAuthority(String authority) {
    this.authority = authority;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Authority authority1 = (Authority) o;
    return Objects.equals(authority, authority1.authority);
  }

  @Override
  public int hashCode() {
    return Objects.hash(authority);
  }
}
