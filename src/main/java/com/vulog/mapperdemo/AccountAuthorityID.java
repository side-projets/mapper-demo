package com.vulog.mapperdemo;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class AccountAuthorityID implements Serializable {
  private String accountUuid;
  private String authorityUuid;

  public AccountAuthorityID() {
  }

  public AccountAuthorityID(String accountUuid, String authorityUuid) {
    this.accountUuid = accountUuid;
    this.authorityUuid = authorityUuid;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AccountAuthorityID that = (AccountAuthorityID) o;
    return Objects.equals(accountUuid, that.accountUuid) &&
      Objects.equals(authorityUuid, that.authorityUuid);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accountUuid, authorityUuid);
  }
}
