package com.vulog.mapperdemo;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class AccountService {

  @Autowired
  private AccountRepository accountRepository;

  @Transactional
  AccountDto convert(String username) {
    Account account = accountRepository.findAccountByUsername(username);
    ModelMapper mapper = new ModelMapper();
    return mapper.map(account, AccountDto.class);
  }
}
