package com.vulog.mapperdemo;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class AccountAuthority {
  @EmbeddedId
  private AccountAuthorityID id;

  @ManyToOne(fetch = FetchType.LAZY)
  @MapsId("accountUuid")
  private Account account;

  @ManyToOne(fetch = FetchType.LAZY)
  @MapsId("authorityUuid")
  private Authority authority;

  // private Date grantData;

  public AccountAuthority() {
  }

  public AccountAuthority(Account account, Authority authority) {
    this.account = account;
    this.authority = authority;

    this.id = new AccountAuthorityID(account.getUuid(), authority.getUuid());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AccountAuthority that = (AccountAuthority) o;
    return Objects.equals(account, that.account) &&
      Objects.equals(authority, that.authority);
  }

  @Override
  public int hashCode() {
    return Objects.hash(account, authority);
  }
}
