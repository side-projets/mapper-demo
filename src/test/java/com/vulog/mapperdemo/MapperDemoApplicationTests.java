package com.vulog.mapperdemo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.MappingException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MapperDemoApplicationTests {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private AccountService accountService;

    @Before
    public void setup() {
        authorityRepository.deleteAll();
        accountRepository.deleteAll();

        // need to flush the changes
        authorityRepository.flush();
        accountRepository.flush();

        Authority admin = new Authority("admin");
        authorityRepository.save(admin);
        Authority dba = new Authority("dba");
        authorityRepository.save(dba);

        Account zhaos = new Account("s.zhao@vulog.com");
        zhaos = accountRepository.save(zhaos);

        zhaos.getAuthorities().add(new AccountAuthority(zhaos, admin));
        zhaos.getAuthorities().add(new AccountAuthority(zhaos, dba));
        accountRepository.save(zhaos);

        accountRepository.flush();
    }

    /**
     * Without transaction, there will be an LazyInitializationException
     */
	@Test(expected = MappingException.class)
    public void modelMapperWithoutTransactionTest() {
        ModelMapper mapper = new ModelMapper();

        Account account = accountRepository.findAccountByUsername("s.zhao@vulog.com");
        mapper.map(account, AccountDto.class);
    }

    /**
     * Although in the service method there is already @Transactional annotation, it will be ignored in TestContext,
     * that's the reason why we need @Transactional here
     *
     * https://docs.spring.io/spring/docs/3.0.5.RELEASE/reference/testing.html#testing-tx
     */
    @Transactional
    @Test
    public void modelMapperWithTransactionTest() {
        accountService.convert("s.zhao@vulog.com");
    }


}
